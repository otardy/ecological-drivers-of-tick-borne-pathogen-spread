# Ecological drivers of tick-borne pathogen spread

This repository includes data that are related to the manuscript:
"Mechanistic movement models reveal ecological drivers of tick-borne pathogen spread"

**DESCRIPTION**

We built a mechanistic movement model to explore how the interplay between host movement, tick and host demographic processes, spatial distribution of resources and the pathogen infection process influences tick-borne pathogen spread dynamics across spatially heterogeneous landscapes. We used the model to generate simulations of tick dispersal by migratory birds and terrestrial hosts across theoretical landscapes varying in resource aggregation, and we performed a sensitivity analysis to explore the impacts of different model input parameters on the rate of infected tick spread, tick infection prevalence and density of infected ticks.

**CONTENT**

- Data:

**TBD_PDE_calibration_analysis.csv**: data file used for the calibration analysis

**TBD_PDE_sensitivity_analysis.csv**: data file used for the sensitivity analysis

**information**: column description



