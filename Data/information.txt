*** Column description ***

param_set_i_rep_j: ID number of the simulation

TI_t_s_1: Rate of infected tick spread with 1% detection threshold (km/day)

TI_t_s_5: Rate of infected tick spread with 5% detection threshold (km/day)

TI_t_s_10: Rate of infected tick spread with 10% detection threshold (km/day)

TI_t_s_20: Rate of infected tick spread with 20% detection threshold (km/day)

mean_inf_prev: Tick infection prevalence (0 – 1)

I_t_max: Maximum density of infected ticks (number of ticks/km²)

phiG: Level of autocorrelation between grid cells of the landscape

prop_adult_ticks: Proportion of adult ticks in the environment (0 – 1)

tick_burden_R: Number of ticks attached to reproduction host (/day)

tfT_plus: Duration of tick feeding on migratory birds and terrestrial hosts (days)

kA0: Maximum carrying capacity of amplification hosts (/km2)

kD0: Maximum carrying capacity of dilution hosts (/km2)

kR0: Maximum carrying capacity of reproduction hosts (/km2)

bA: Birth rate of amplification hosts (/day)

bD: Birth rate of dilution hosts (/day)

bR: Birth rate of reproduction hosts (/day)

dA: Natural mortality rate of amplification hosts (/day)

dD: Natural mortality rate of dilution hosts (/day)

dR: Natural mortality rate of reproduction hosts (/day)

dT: Mortality rate of ticks (/day)

BT: Number of eggs produced by fed adult female tick (/day)

betaAT: Probability that an infected amplification host transmits the pathogen to a susceptible tick (0 – 1)

betaDT: Probability that an infected dilution host transmits the pathogen to a susceptible tick (0 – 1)

betaTA: Probability that an infected tick transmits the pathogen to a susceptible amplification host (0 – 1)

betaTD: Probability that an infected tick transmits the pathogen to a susceptible dilution host (0 – 1)

alphaTA0: Base rate at which immature ticks encounter amplification host (/day)

alphaTD0: Base rate at which immature ticks encounter dilution host (/day)

alphaTR0: Base rate at which adult ticks encounter reproduction host (/day)

thetaT_plus: Allee effect threshold (0 – 1)

rhoD_migration: Distance moved by migratory birds per day (km)

rhoH_breeding: Distance moved by terrestrial hosts per day (km)

kappaD_migration: Concentration parameter of the von Mises distribution describing the magnitude of directional bias towards the north or south for migratory birds

kappaH_breeding: Concentration parameter of the von Mises distribution describing the magnitude of directional bias towards the north or south for terrestrial hosts

psiT_migration_N: Weighting factor that controls the proportion of ticks moving to the north by migratory birds (0 – 1)

psiT_migration_S: Weighting factor that controls the proportion of ticks moving to the south by migratory birds (0 – 1)

sigmaG: Rate at which the daily distance moved by ticks on migratory birds and terrestrial hosts decreases with increasing resource availability